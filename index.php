<!DOCTYPE html>
<html lang="en">
<head>
<title>FredGen - SSG</title>
</head>

<style>
body {
    font-family: helvetica, arial, sans;
}
</style>

<body>
<center>
    <h1>FredGen - Static Site Generator</h1>

    <p>
        Click the button below to compile your static site.
    </p>

    <a href='?build'>
        <button>Compile Site</button>
    </a>


<?php

// Start Page Build
if (isset($_REQUEST['build'])) {

// Config Include
include ('config.php');

// Setup Directorys
include ('lib/setupfolders.php');

// Build website pages
include ('lib/build-pages.php');

// Build a sitemap
if ($SitemapEnable) include ('lib/sitemap.php');

// Build a robots.txt file
if ($RobotsTxtEnable) include ('lib/robots.php');

// Build a htaccess file
if ($htaccessEnable) include ('lib/htaccess.php');

// Build Blog
if ($BlogEnable) include ('lib/blog.php');

// Build RSS feed from blog posts
if ($RSSEnable) include ('lib/rss.php');




// Tabs to spaces (ffs)
$t = '    '; // 4 SPACES !

// echo complete
echo "$t<p>\n$t{$t}Build Complete\n$t</p>";

}

?>


</center>
</body>

</html>
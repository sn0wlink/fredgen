# FredGen

A small FOSS static site generator. What more can be said?

# Licence
GNU General Public License v3.0

# Features
- Simple build procedure
- Fast build time
- Lightweight design
- Markdown support

# Contribution Rules
- 4 Spaces - NOT tabs... (please)
- 80 char width soft limit (it just looks nice)
- Be excellent to each other! (it's the LAW, so deal with it.)

# Contributions
With thanks to Viperfang for his awsome code and many merges. No idea how it works, it just does.

[Viperfang Gitlab](https://gitlab.com/viperfang)
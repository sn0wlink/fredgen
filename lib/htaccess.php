<?php

/*
    Application: htaccess file creator
    Author: David Collins-Cubitt
    Description: Robots.txt file generator
    Date: 01/06/2020

*/

// Check if enabled


if ($Error404Enable) {
    $htaccessOutput = "ErrorDocument 404 $SiteBaseURL/Error404.html";
}

if ($DomainRedirectEnable) {
$htaccessOutput .= "
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME}.html -f
RewriteRule ^(.+)$ $1.html [L]
";
}

// Write to file
file_put_contents("$StorageLocation/.htaccess", $htaccessOutput);

?>
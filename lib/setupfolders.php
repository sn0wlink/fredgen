<?php

/*
    Setup directories for build engine
*/

// Generate Full file path
$WebsitePath = realpath("$StorageLocation");

if (!file_exists("$WebsitePath/images")) {
    mkdir("$WebsitePath/images");
}

if (!file_exists("$WebsitePath/media")) {
    mkdir("$WebsitePath/media");
}

if (!file_exists("$WebsitePath/blog")) {
    mkdir("$WebsitePath/blog");
}

if (!file_exists("$WebsitePath/blog/images")) {
    mkdir("$WebsitePath/blog/images");
}

if (!file_exists("$WebsitePath/blog/media")) {
    mkdir("$WebsitePath/blog/media");
}

?>
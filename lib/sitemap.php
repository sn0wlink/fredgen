<?php

/*
    Application: Sitemap
    Author: David Collins-Cubitt
    Description: Sitemap Generator Script
    Date: 01/06/2020

*/

// Check if sitemap is enabled

$Sitemap = "
<?xml version='1.0' encoding='UTF-8/?>
<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>
";

foreach (glob("$StorageLocation/*") as $filepath) {
    
    // Create / trim the filename (readable)
    $filename = str_replace("$StorageLocation/", "", $filepath);
    
    // Last Updated
    $SitemapDatetime = Date("Y-m-d\TG:i:sP"); // Yeah,

    $SitemapPriority = "0.5";

    if ($filename=="index.html") {
        $SitemapPriority = "1.0";
    }

    $Sitemap .= "
<url>
    <loc>$SiteBaseURL$filename</loc>
    <lastmod>$SitemapDatetime</lastmod>
    <changefreq>$SitemapUpdates</changefreq>
    <priority>$SitemapPriority</priority>
</url>
    ";
}

$Sitemap .= "
</urlset>";

file_put_contents("$StorageLocation/sitemap.xml", $Sitemap);

?>
<?php 

/*
    Application: RSS Builder
    Author: David Collins-Cubitt
    Description: Builds a RSS feed from blog posts
    Date: 02/06/2020
*/

// Start Buffer
ob_start();

echo "
<?xml version='1.0' encoding='utf-8' ?> 
<rss version='2.0' xml:base='$SiteBaseURL/blog' xmlns:atom='http://www.w3.org/2005/Atom' xmlns:dc='http://purl.org/dc/elements/1.1/' xmlns:media='http://search.yahoo.com/mrss/'>

<channel>

<title>$SiteName</title>
<link>$SiteBaseURL</link>
<description>$SiteDescription</description>
";

// Search for pages & build index
foreach (glob("content/blog/posts/*") as $filename) {

    // Get blog content and convert markdown to plain text
    $RssBlogContent = file_get_contents($filename);
    $RssBlogContent = $Parsedown->text($RssBlogContent);
    $RssBlogContent = strip_tags($RssBlogContent);

    $filename = str_replace('content/blog/posts/', '', $filename);
    $htmlFilename = str_replace('.md', '.html', $filename);
    $RssBlogTitle = substr($filename, 9);
    $RssBlogTitle = str_replace('.md', '', $RssBlogTitle);

    // Make post date readable
    $PostDateYear = substr($filename, 0, 4);
    $PostDateMonth = substr($filename, 4, 2);
    $PostDateDay = substr($filename, 6, 2);

    // A weird way to implement a publication date I admit, 
    // but what the hell...
    $pubDate = "$PostDateDay" . "-" . "$PostDateMonth" . "-" . "$PostDateYear"; 
    $pubDate= date("D, d M Y h:i:s T", strtotime($pubDate));

    // Start making the post!
    echo "
    <item>
        <title>$RssBlogTitle</title>
        <author>$AuthorName</author>
        <pubDate>$pubDate</pubDate>
        <language>en-us</language>
        <link>$SiteBaseURL/blog/$htmlFilename</link>
        <description>$RssBlogContent</description>
    </item>
    ";
}

echo "
</channel>
</rss>
";

// Write output buffer to variable
$PageOutput = ob_get_contents();

// Flush output buffer
ob_end_clean(); 

// Dump page to file
$FeedLocation = "$StorageLocation/blog/feed.rss";
file_put_contents($FeedLocation, $PageOutput);

?>
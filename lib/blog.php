<?php 

/*
    Application: Blog Makers
    Author: David Collins-Cubitt
    Description: Blog compiler
    Date: 02/06/2020

*/

// Setup config variables
include ('config.php');

// Remove stored static site pages
foreach (glob("$StorageLocation/blog/*") as $filename) {
    @unlink($filename);
}

// Remove stored static site images
foreach (glob("$StorageLocation/blog/images/*") as $filename) {
    @unlink($filename);
}

// Copy all images
foreach (glob("content/blog/images/*") as $filename) {
    $NewFilename = str_replace("content/blog/images/", "", $filename);
    copy($filename, "$StorageLocation/blog/images/$NewFilename");
}

// Search for pages & build
foreach (glob("content/blog/posts/*") as $filename) {

    // You can't do folders!
    if(is_dir($filename)) continue;

    $filename = basename($filename);
    $fileparts = explode('.',$filename);
    $fileext = array_pop($fileparts);
    $filename = implode(".",$fileparts);

    // Setup Page Name
    $PageName = $filename;

    $PageName = ucfirst($PageName);

    // Check if index.php
    if ($filename == "index") {
        $PageName = $HomePageName;
    }

    // Start Buffer
    ob_start();

    // Get contents
    $Style = file_get_contents("content/style.css");

    // Just don't ask, It works - Viperfang Networks (Your Welcome!)
    // https://gitlab.com/viperfang
    $Menu = file_get_contents("content/menu.php"); //FM
    eval('$Menu = "' .$Menu. '";'); // AM
    
    $Content = file_get_contents("content/blog/posts/$filename.$fileext");
    
    switch($fileext) {
        case "html": // Do nothing, its already perfect
            break;
        case "php":
            eval('?>'.$Content.'<?php ');
            $Content = ob_get_contents();
            ob_clean();
            break;
        case "md": // Markdown!

            // Run The Sn0wlink MD Converter
            include ('sn0wlink-markdown.php');

            // Run the Official MD Converter
            $Content = $Parsedown->text($Content);

            break;
        default:
            break;
    }

    // Build Layout
    include ('content/layout.php');

    // Write buffer to variable
    $PageOutput = ob_get_contents();

    // flush output buffer
    ob_end_clean(); 

    // Dump page to file
    $NewFilename = "$StorageLocation/blog/$filename.html";
    file_put_contents($NewFilename, $PageOutput);
}

///////////////////////////////////////////////////////////////
//  Build Blog Index Page
///////////////////////////////////////////////////////////////

// Clear variables
$Content = "";

// Start Buffer
ob_start();

$Style = file_get_contents("content/style.css");

// Just don't ask, It works - Viperfang Networks (Your Welcome!)
// https://gitlab.com/viperfang
$Menu = file_get_contents("content/menu.php"); //FM
eval('$Menu = "' .$Menu. '";'); // AM

$Content = "<h1>Recent Posts</h1> \n";

// Search for pages & build index
foreach (array_reverse(glob("content/blog/posts/*")) as $filename) {

    $filename = str_replace("content/blog/posts/", '', $filename);
    $htmlFilename = str_replace('.md', '', $filename); // removes ext
    $htmlFilename = str_replace('.html', '', $htmlFilename); // removes ext
    $htmlFilename = "$htmlFilename.html"; // Adds .html ext
    $BlogTitle = substr($filename, 8); // Removes date stamp
    $BlogTitle = str_replace('.md', '', $BlogTitle);
    $BlogTitle = str_replace('.html', '', $BlogTitle);

    // Make post date readable
    $PostDateYear = substr($filename, 0, 4);
    $PostDateMonth = substr($filename, 4, 2);
    $PostDateDay = substr($filename, 6, 2);

    $Content .= "<a href='$htmlFilename'> \n";
    $Content .= "<strong>$PostDateDay/$PostDateMonth/$PostDateYear - $BlogTitle</strong>\n";
    $Content .= "</a><br />\n";
}

$Content .= "<p><a href='$SiteBaseURL/blog/feed.rss'>RSS Feed</a></p> \n";

// Build Layout
include ('content/layout.php');

// Write output buffer to variable
$PageOutput = ob_get_contents();

// Flush output buffer
ob_end_clean(); 

// Dump page to file
$IndexLocation = "$StorageLocation/blog/index.html";
file_put_contents($IndexLocation, $PageOutput);

?>
<?php

/*
$Tag[0] !video=100%[New Video File](media/example.mp4)
$Tag[1] video
$Tag[2] 100%
$Tag[3] New Video File
$Tag[4] media/example.mp4
*/

// Search for string
$pattern = '/\!([A-Za-z0-9]*)=*(.*)\[(.*)\]\((.*)\)/';
preg_match_all ($pattern, $Content, $Tags, PREG_SET_ORDER);

foreach ($Tags as $Tag) {

if ($Tag[2] != "") $StyleWidth = "style='width: $Tag[2];'";
else $StyleWidth = "";

// DO NOT TAB IN !
$TagOutput = "
<$Tag[1] $StyleWidth controls>
<source src='$Tag[4]'>
Your browser does not support the $Tag[1] element file: $Tag[3]
</$Tag[1]>
";

    $Content = str_replace($Tag[0],$TagOutput, $Content);
}

?>
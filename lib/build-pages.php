<?php

// Remove stored static site pages
foreach (glob("$StorageLocation/*") as $filename) {
    @unlink($filename);
}

// Remove stored static site images
foreach (glob("$StorageLocation/images/*") as $filename) {
    @unlink($filename);
}


// Include Markdown Conversion Library
include ('parsedown.php');
$Parsedown = new Parsedown();

// Copy all images
foreach (glob("content/images/*") as $filename) {
    $NewFilename = str_replace("content/images/", "", $filename);
    copy($filename, "$StorageLocation/images/$NewFilename");
}

// Copy all images
foreach (glob("content/blog/images/*") as $filename) {
    $NewFilename = str_replace("content/blog/images/", "", $filename);
    copy($filename, "$StorageLocation/blog/images/$NewFilename");
}

// Search for pages & build
foreach (glob("content/pages/*") as $filename) {

    // You can't do folders!
    if(is_dir($filename)) continue;

    $filename = basename($filename);
    $fileparts = explode('.',$filename);
    $fileext = array_pop($fileparts);
    $filename = implode(".",$fileparts);

    // Setup Page Name
    $PageName = $filename;

    $PageName = ucfirst($PageName);

    // Check if index.php
    if ($filename == "index") {
        $PageName = $HomePageName;
    }

    // Start Buffer
    ob_start();

    // Get contents
    $Style = file_get_contents("content/style.css");
    
    // Just don't ask, It works - Viperfang Networks (Your Welcome!)
    // https://gitlab.com/viperfang
    $Menu = file_get_contents("content/menu.php"); //FM
    eval('$Menu = "' .$Menu. '";'); // AM
    
    $Content = file_get_contents("content/pages/$filename.$fileext");
    
    switch($fileext) {
        case "html": // Do nothing, its already perfect
            break;
        case "php":
            eval('?>'.$Content.'<?php ');
            $Content = ob_get_contents();
            ob_clean();
            break;
        case "md": // Markdown!

            // Run The Sn0wlink MD Converter
            include ('sn0wlink-markdown.php');

            // Run the Official MD Converter
            $Content = $Parsedown->text($Content);
            
            break;
        default:
            break;
    }

    // Build Layout
    include ('content/layout.php');

    $PageOutput = ob_get_contents();
    ob_end_clean(); 

    // Dump page to file
    $NewFilename = "$StorageLocation/$filename.html";
    file_put_contents($NewFilename, $PageOutput);
}

?>
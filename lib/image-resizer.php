<?php

/*
    Application: Simple Image Resizer
    Author: David Collins-Cubitt
    Description: A simple image resize function
*/

function ResizeImage($Source, $Destination, $Height, $Width) {
    
    // Content type
    header('Content-Type: image/jpeg');

    // Get new dimensions
    list($OriginalWidth, $OriginalHeight) = getimagesize($Source);

    $ImageRatio = $OriginalWidth/$OriginalHeight;

    if ($Width/$Height > $ImageRatio) {
       $Width = $Height*$ImageRatio;
    } 

    else {
       $Height = $Width/$ImageRatio;
    }

    // Resample
    $image_p = imagecreatetruecolor($Width, $Height);
    $Image = imagecreatefromjpeg($filename);
    imagecopyresampled($image_p, $Image, 0, 0, 0, 0, $Width, $Height, $OriginalWidth, $OriginalHeight);

    // Output
    imagejpeg($image_p, null, 100);

?>
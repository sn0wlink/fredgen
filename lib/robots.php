<?php

/*
    Application: Robots
    Author: David Collins-Cubitt
    Description: Robots.txt file generator
    Date: 01/06/2020

*/

// Check if enabled

    $RobotsOutput = '';
    
    // If web crawler is enabled
    if ($AllowWebCrawler) {
        $RobotsOutput .= "User-agent: *\nDisallow:\n";
    }

    // If web crawler is disabled
    else {
        $RobotsOutput .= "User-agent: *\nDisallow: /\n";
    }

    // Append Sitemap
    $RobotsOutput .= "Sitemap: $SiteBaseURL/sitemap.xml";

    // Write to file
    file_put_contents("$StorageLocation/robots.txt", $RobotsOutput);

?>
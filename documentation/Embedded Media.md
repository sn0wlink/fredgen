#Adding Media
You can now add media to your posts by using the following tags
    
    !video[New Video File](media/example-video.mp4)
    !audio[New Video File](media/example-audio.mp3)

The width of the video/audio can be changed by adding *!video* or *!audio* after 
the filtype:

    !video=100%[New Video File](media/example-video.mp4)
    !audio=400px[New Video File](media/example-audio.mp3)

Supported video filetypes are:
- MP4
- WebM
- Ogg

Supported video filetypes are:
- MP3
- Ogg
- WAV

Images can be added in the standard Markdown format:

![Image Name](images/example-image.jpg)
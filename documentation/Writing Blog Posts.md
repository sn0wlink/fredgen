#File Naming Convention
The filename consists of two main parts, The date stamp and the blog post title.
These should be as follows with a single space after the date stamp.

    YYYYMMDD Blog Post Name.ext

the '.ext' can be either a '.md' or a '.html' depending on what you post is
written in.

#Writing your blog post
You can use markdown or html. Both should work ok.


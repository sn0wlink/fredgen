<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$SiteName?> - <?=$PageName?></title>
    <meta charset="UTF-8">
    <meta name="description" content="<?=$SiteDescription ?>">
    <meta name="keywords" content="<?=$SiteKeywords?>">
    <meta name="author" content="<?=$AuthorName?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
</head>

<style>
    <?=$Style?>

</style>

<body>

<div class="menu">
<?=$Menu?>
</div>

<div class="content">
<?=$Content?>
</div>

<div class="footer">
    <hr />
    <br />
    Last Updated <?=$LastUpdated?> By <?=$AuthorName?><br />
    Content on this site is Copyright 2020 &copy;
<div>

</body>

</html>
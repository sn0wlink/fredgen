<?php

// Site Name
$SiteName = "FredGen";

// Website Base Url
$SiteBaseURL = "http://192.168.0.213/fredgen/static-site"; // https://example.com

// Website description for SEO and RSS
$SiteDescription = "FredGen Static Site Generator";

$SiteKeywords = "Static, Site, Generator, Builder";

// Author Name
$AuthorName = "David Collins-Cubitt";

// Homepage Name
$HomePageName = "Homepage";

// Build a blog
$BlogEnable = TRUE; // TRUE, FALSE

// Build a RSS feed
$RSSEnable = TRUE;

// Auto generate date
$LastUpdated = Date("d-m-Y");

// Static Site Storage Location
$StorageLocation = "static-site";

// Enable htaccess file
$htaccessEnable = TRUE;

// Enable custom Error 404 Page
$Error404Enable = TRUE;

// Enable short urls, ../contact -> ../contact.html
$DomainRedirectEnable = TRUE;

// Enable robots.txt file
$RobotsTxtEnable = TRUE;

// Appear in search engine results
$AllowWebCrawler = TRUE;

// Enable Sitemap
$SitemapEnable = TRUE; // TRUE, FALSE

// Update period - for sitemap
$SitemapUpdates = "monthly"; // always, hourly, daily, weekly, monthly, yearly, never

?>